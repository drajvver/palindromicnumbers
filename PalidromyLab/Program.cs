﻿//Largest palindrome product

//A palindromic number reads the same both ways.The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

//Find the largest palindrome made from the product of two 3-digit numbers.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PalidromyLab
{
    /// <summary>
    /// Offers basic information about Palindromic Number
    /// </summary>
    public class PalindromicNumber : IComparable<PalindromicNumber>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="firstNumber">First component number</param>
        /// <param name="secondNumber">Second component number</param>
        /// <param name="finalNumber">Result of multiplication of first and second number</param>
        public PalindromicNumber(int firstNumber, int secondNumber, int finalNumber)
        {
            FirstNumber = firstNumber;
            SecondNumber = secondNumber;
            FinalNumber = finalNumber;
        }
        public int FirstNumber { get; set; }
        public int SecondNumber { get; set; }
        public int FinalNumber { get; set; }

        /// <summary>
        /// Compares Two Palindronic Numbers, returning this one which has higher final value
        /// </summary>
        /// <param name="other">Palindromic Number to compare to</param>
        /// <returns>Bigger PalindromicNumber</returns>
        public int CompareTo(PalindromicNumber other)
        {
            return other.FinalNumber > this.FinalNumber ? other.FinalNumber : this.FinalNumber;
        }

        /// <summary>
        /// Checkts wether a number is palindromic
        /// </summary>
        /// <param name="number">Palindromic Number converted to string</param>
        /// <returns>True - if is palindromic number, else false</returns>
        public static bool IsPalindromicNumber(string number)
        {
            var numb = number.ToCharArray();
            if (numb.Length < 6)
                return false;
            if (numb[0] == numb[5] && numb[1] == numb[4] && numb[2] == numb[3])
                return true;

            return false;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var listOfPalindromes = new List<PalindromicNumber>();

            for (int i = 100; i < 1000; i++)
            {
                for (int j = 100; j < 1000; j++)
                {
                    if (PalindromicNumber.IsPalindromicNumber((i * j).ToString()))
                    {
                        listOfPalindromes.Add(new PalindromicNumber(i, j, i*j));
                    }
                }
            }

            var biggestNumber = listOfPalindromes.OrderByDescending(t => t.FinalNumber).First();

            Console.WriteLine("Biggest palindrome is: {0} * {1} = {2}", biggestNumber.FirstNumber, biggestNumber.SecondNumber, biggestNumber.FinalNumber);
            Console.Read();
        }
    }

    
}
